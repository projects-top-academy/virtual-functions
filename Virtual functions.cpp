﻿#include <iostream>
#include <string>

class Fruit {
private:
    double weight;
    double price;
    std::string name;

public:
    Fruit(double weight, double price, const std::string& name) : weight(weight), price(price), name(name) {}

    double getWeight() const { return weight; }
    double getPrice() const { return price; }
    std::string getName() const { return name; }

    virtual void print() const = 0;
};

class Apple : public Fruit {
public:
    enum Color {Green, Yellow, Red};

private:
    Color color;

public:
    Apple(double weight, double price, const std::string& name, Color color) : Fruit(weight, price, name), color(color) {}

    Color getColor() const { return color; }

    void print() const override {
        std::cout << "Name: " << getName() << ", Weight: " << getWeight() << " kg, Price: rub " << getPrice() << ", Color: ";
        switch (color)
        {
        case Apple::Green:
            std::cout << "Green";
            break;
        case Apple::Yellow:
            std::cout << "Yellow";
            break;
        case Apple::Red:
            std::cout << "Red";
            break;
        }
        std::cout << "\n";
    }
};

class Banana : public Fruit {
public:
    enum class Ripeness {Unripe, Ripe};

private:
    Ripeness ripeness;

public:
    Banana(double weight, double price, const std::string& name, Ripeness ripeness) : Fruit(weight, price, name), ripeness(ripeness) {}

    Ripeness getRipeness() const { return ripeness; }

    void print() const override {
        std::cout << "Name: " << getName() << ", Weight: " << getWeight() << " kg, Price: rub " << getPrice() << ", Ripeness: ";
        switch (ripeness)
        {
        case Banana::Ripeness::Unripe:
            std::cout << "Unripe";
            break;
        case Banana::Ripeness::Ripe:
            std::cout << "Ripe";
            break;
        }
        std::cout << "\n";
    }

};

class Grape : public Fruit {
public:
    Grape(double weight, double price, const std::string& name) : Fruit(weight, price, name) {}

    void print() const override {
        std::cout << "Name: " << getName() << ", Weight: " << getWeight() << " kg, Price: rub " << getPrice() << "\n";
    }
};

int main()
{
    Fruit* fruits[3];

    fruits[0] = new Apple(0.2, 1.5, "Apple", Apple::Color::Red);
    fruits[1] = new Banana(0.15, 1.0, "Banana", Banana::Ripeness::Ripe);
    fruits[2] = new Grape(0.1, 2.0, "Grape");

    for (int i = 0; i < 3; ++i)
    {
        fruits[i]->print();
    }

    for (int i = 0; i < 3; ++i)
    {
        delete fruits[i];
    }

    return 0;
}